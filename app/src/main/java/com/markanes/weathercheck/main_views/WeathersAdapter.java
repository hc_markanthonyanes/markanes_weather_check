package com.markanes.weathercheck.main_views;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.markanes.weathercheck.R;

import java.util.List;

/**
 * Created by Mark on 8 Nov 2018.
 */

public class WeathersAdapter extends RecyclerView.Adapter<WeathersAdapter.MyViewHolder> {

    private List<Weather> weathersList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, location, weather, temp;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.tv_name);
            location = (TextView) view.findViewById(R.id.tv_location);
            weather = (TextView) view.findViewById(R.id.tv_weather);
            temp = (TextView) view.findViewById(R.id.tv_temp);

        }
    }


    public WeathersAdapter(List<Weather> weathersList) {
        this.weathersList = weathersList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Weather Weather = weathersList.get(position);
        holder.name.setText(Weather.getName());
        holder.location.setText(Weather.getLocation());
        holder.weather.setText(Weather.getWeather());
        holder.temp.setText(Weather.getTemp());
    }

    @Override
    public int getItemCount() {
        return weathersList.size();
    }
}
