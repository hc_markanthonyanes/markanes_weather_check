package com.markanes.weathercheck.main_views;

/**
 * Created by Mark on 8 Nov 2018.
 */

public class Weather {
    private String name;
    private String location;
    private String weather;
    private String temp;

    public Weather() {
    }

    public Weather(String name, String location, String weather, String temp) {
        this.name = name;
        this.location = location;
        this.weather = weather;
        this.temp = temp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
