package com.markanes.weathercheck;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.markanes.weathercheck.database.DatabaseHandler;

public class Splash extends AppCompatActivity {

    private TextView tv_splash;
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.splash);

        if (BuildConfig.FLAVOR.equals("Dev")) {
            setContentView(R.layout.splash_dev);
            tv_splash = findViewById(R.id.tv_splash);
            tv_splash.setText(getResources().getString(R.string.tv_splash) + " (" + BuildConfig.FLAVOR + ")" +
                    "\nv "+BuildConfig.VERSION_NAME);
        }

        if (BuildConfig.FLAVOR.equals("Test")) {
            setContentView(R.layout.splash);
            tv_splash = findViewById(R.id.tv_splash);
            tv_splash.setText(getResources().getString(R.string.tv_splash) + " (" + BuildConfig.FLAVOR + ")" +
                    "\nv "+BuildConfig.VERSION_NAME);
        }

        if (BuildConfig.FLAVOR.equals("Production")) {
            setContentView(R.layout.splash_production);
            tv_splash = findViewById(R.id.tv_splash);
            tv_splash.setText(getResources().getString(R.string.tv_splash) +
                    "\nv "+BuildConfig.VERSION_NAME);
        }

        DatabaseHandler db = new DatabaseHandler(this);
        // create / open tables
        db.getWritableDatabase();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the MainActivity. */
                Intent mainIntent = new Intent(Splash.this, Main.class);
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
