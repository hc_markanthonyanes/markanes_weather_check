package com.markanes.weathercheck;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.markanes.weathercheck.database.DatabaseHandler;
import com.markanes.weathercheck.database.Tables;
import com.markanes.weathercheck.main_views.RecyclerTouchListener;
import com.markanes.weathercheck.main_views.Weather;
import com.markanes.weathercheck.main_views.WeathersAdapter;
import com.markanes.weathercheck.methods.Methods;
import com.markanes.weathercheck.weather_details.Weather_Detail_Screen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mark on 8 Nov 2018.
 */

public class Main extends AppCompatActivity {

    private List<Weather> weatherList = new ArrayList<>();
    private RecyclerView recyclerView;
    private WeathersAdapter mAdapter;

    DatabaseHandler db = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db.getWritableDatabase();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new WeathersAdapter(weatherList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Weather weather = weatherList.get(position);

//                Toast.makeText(getApplicationContext(), weather.getLocation() + " is selected!", Toast.LENGTH_SHORT).show();

                /*Intent i = new Intent(Main.this, Weather_Detail_Screen.class);
                startActivity(i);*/

                startActivity(new Intent(Main.this, Weather_Detail_Screen.class)
                        .putExtra("name", weather.getName())
                        .putExtra("location", weather.getLocation())
                        .putExtra("weather", weather.getWeather())
                        .putExtra("temp", weather.getTemp())
                );
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        final FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                weatherList.clear();
                mAdapter.notifyDataSetChanged();
                fetch();

            }
        });




        AndroidNetworking.initialize(getApplicationContext());
        fetch();

        if (ActivityCompat.checkSelfPermission(Main.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Main.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Main.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }else{
            // Write you code here if permission already given.
        }

    }

    void fetch(){
        if (new Methods().isConnected(Main.this)) {
            getResponse("London");
            getResponse("Prague");
            getResponse("San Francisco");

        } else {

            if (db.getRowCount(Tables.tbl_weather.table_name) != 0) {
                for (int x = 1; x <= 3; x++) {
                    fetchOfflineData(x);
                }
            }

        }
    }


    private void getResponse(final String city){

        AndroidNetworking.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&APPID=1eb12e9dc21d13e9b8a522005246d10b")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        tv_response.setText(response.toString());
                        Log.e("response",response.toString());

                        HashMap<String, String> hashResponse = new Methods().JSONtoHash(response);
                        try {
                            String name = city;
                            Log.e("long", response.getJSONObject("coord").optString("lon"));
                            Log.e("lat", response.getJSONObject("coord").optString("lat"));
                            String location = response.getJSONObject("coord").optString("lon") +","+ response.getJSONObject("coord").optString("lat");

                            JSONArray weatherAry = new JSONArray();
                            weatherAry = response.getJSONArray("weather");
                            Log.e("weather", weatherAry.toString() );
                            Log.e("weather", response.getJSONArray("weather").get(0).toString() );
                            Log.e("description", new JSONObject( response.getJSONArray("weather").get(0).toString() ).optString("description"));
                            String weather = new JSONObject( response.getJSONArray("weather").get(0).toString() ).optString("description");

                            Log.e("temp", response.getJSONObject("main").optString("temp"));
                            String temp = response.getJSONObject("main").optString("temp");

                            //add data to recyclerview
                            Weather wthr = new Weather(name, location, weather, temp);
                            weatherList.add(wthr);
                            mAdapter.notifyDataSetChanged();

                            //add to db
                            HashMap<String, String> data = new HashMap<>();
                            data.put("name_id", hashResponse.get("name_id"));
                            data.put("name", hashResponse.get("name"));
                            data.put("coord_lon", hashResponse.get("coord_lon"));
                            data.put("coord_lat", hashResponse.get("coord_lat"));
                            data.put("weather_description", hashResponse.get("weather_description"));
                            data.put("weather_icon", hashResponse.get("weather_icon"));
                            data.put("main_temp", hashResponse.get("main_temp"));
                            data.put("main_pressure", hashResponse.get("main_pressure"));
                            data.put("main_humidity", hashResponse.get("main_humidity"));
                            data.put("wind_speed", hashResponse.get("wind_speed"));
                            data.put("wind_deg", hashResponse.get("wind_deg"));
                            data.put("clouds_all", hashResponse.get("clouds_all"));
                            data.put("sys_sunrise", hashResponse.get("sys_sunrise"));
                            data.put("sys_sunset", hashResponse.get("sys_sunset"));


                            String checkName = db.getRecord("name", Tables.tbl_weather.table_name,"name", name);
                            if (checkName.contentEquals("")) {
                                Log.e("add",data.toString());
                                db.addRecord(data, Tables.tbl_weather.table_name);
                            } else {
                                Log.e("update",data.toString());
                                db.updateRecord(data, "name = ?", new String[] {name}, Tables.tbl_weather.table_name);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            Log.e("errorCode", "onError errorCode : " + error.getErrorCode());
                            Log.e("errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.e("errorDetail", "onError errorDetail : " + error.getErrorDetail());

                        } else {
                            Log.e("errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }

                });

    }

    private void fetchOfflineData(int id){
        HashMap<String, String> data =  db.getRecord("id = ?", new String[] {String.valueOf(id)}, Tables.tbl_weather.table_name);
        Log.e("data offline", data.toString());

        //add data to recyclerview
        Weather wthr = new Weather(
                data.get("name"),
                data.get("coord_lon")+","+data.get("coord_lat"),
                data.get("weather_description"),
                data.get("main_temp"));
        weatherList.add(wthr);
        mAdapter.notifyDataSetChanged();

    }


}

