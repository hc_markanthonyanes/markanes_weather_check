package com.markanes.weathercheck.weather_details;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.markanes.weathercheck.R;
import com.markanes.weathercheck.database.DatabaseHandler;
import com.markanes.weathercheck.database.Tables;
import com.markanes.weathercheck.main_views.RecyclerTouchListener;
import com.markanes.weathercheck.main_views.Weather;
import com.markanes.weathercheck.methods.Methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mark on 8 Nov 2018.
 */

public class Weather_Detail_Screen extends AppCompatActivity {
    /**
     * icon code
     http://openweathermap.org/img/w/04n.png
     */


    /**
     * http://api.openweathermap.org/data/2.5/weather?q=Manila&APPID=ea574594b9d36ab688642d5fbeab847e
     *
     *Parameters:
     coord
         coord.lon City geo location, longitude
         coord.lat City geo location, latitude
     weather (more info Weather condition codes)
         weather.id Weather condition id
         weather.main Group of weather parameters (Rain, Snow, Extreme etc.)
         weather.description Weather condition within the group
         weather.icon Weather icon id
     base Internal parameter
     main
         main.temp Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
         main.pressure Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
         main.humidity Humidity, %
         main.temp_min Minimum temperature at the moment. This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
         main.temp_max Maximum temperature at the moment. This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
         main.sea_level Atmospheric pressure on the sea level, hPa
         main.grnd_level Atmospheric pressure on the ground level, hPa
     wind
         wind.speed Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
         wind.deg Wind direction, degrees (meteorological)
     clouds
        clouds.all Cloudiness, %
     rain
        rain.3h Rain volume for the last 3 hours
     snow
        snow.3h Snow volume for the last 3 hours
     dt Time of data calculation, unix, UTC
     sys
        sys.type Internal parameter
        sys.id Internal parameter
        sys.message Internal parameter
         sys.country Country code (GB, JP etc.)
         sys.sunrise Sunrise time, unix, UTC
         sys.sunset Sunset time, unix, UTC
     id City ID
     name City name
     cod Internal parameter
     */


    AppBarLayout app_bar;
    TextView tv_title, tv_subtitle1, tv_subtitle2, tv_subtitle3;
    String name, location, weather, temp;

    TextView tv_message;
    ImageView imageView;

    DatabaseHandler db = new DatabaseHandler(this);

    private List<Weather_Details> weatherList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Weather_Detail_Adapter mAdapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather_detail_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tv_title = findViewById(R.id.tv_title);
        tv_subtitle1 = findViewById(R.id.tv_subtitle1);
        tv_subtitle2 = findViewById(R.id.tv_subtitle2);
        tv_subtitle3 = findViewById(R.id.tv_subtitle3);
        tv_message = findViewById(R.id.tv_message);
        imageView = findViewById(R.id.imageView);

        db.getWritableDatabase();

        name = getIntent().getStringExtra("name");
        location = getIntent().getStringExtra("location");
        weather = getIntent().getStringExtra("weather");
        temp = getIntent().getStringExtra("temp");

//        Toast.makeText(getApplicationContext(), location + "\n" + weather + "\n" + temp , Toast.LENGTH_SHORT).show();

        //detect expanded collapsing toolbar
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        app_bar = (AppBarLayout) findViewById(R.id.app_bar);
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            //            boolean showMenu = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(name);
//                    setTitle(account_name);
                    showMenu = true;
                } else if(showMenu) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
//                    setTitle(" ");
                    tv_title.setText(name);
                    tv_subtitle1.setText( location );
                    tv_subtitle2.setText( weather );
                    tv_subtitle3.setText( temp );
                    showMenu = false;
                }
                invalidateOptionsMenu();//recall onCreateOptionsMenu
            }
        });

        final FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetch();
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new Weather_Detail_Adapter(weatherList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Weather_Details weather_details = weatherList.get(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        fetch();

    }

    void fetch(){
        weatherList.clear();
        mAdapter.notifyDataSetChanged();

        if (new Methods().isConnected(Weather_Detail_Screen.this)) {

            getResponse(name);

        } else {

            fetchOfflineData(name);

        }
    }

    boolean showMenu = true;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_weather_detail_screen, menu);
        if (!showMenu) {
            menu.findItem(R.id.action_refresh).setVisible(false);
        }
        if (showMenu) {
            menu.findItem(R.id.action_refresh).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        if (item.getItemId() == R.id.action_refresh) {
            fetch();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    void getResponse(final String city){

        AndroidNetworking.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&APPID=1eb12e9dc21d13e9b8a522005246d10b")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        tv_response.setText(response.toString());
                        Log.e("response",response.toString());
                        tv_message.setText(response.toString());

                        try {
                            String name = city;
                            Log.e("long", response.getJSONObject("coord").optString("lon"));
                            Log.e("lat", response.getJSONObject("coord").optString("lat"));
                            String location = response.getJSONObject("coord").optString("lon") +","+ response.getJSONObject("coord").optString("lat");

                            JSONArray weatherAry = new JSONArray();
                            weatherAry = response.getJSONArray("weather");
                            Log.e("weather", weatherAry.toString() );
                            Log.e("weather", response.getJSONArray("weather").get(0).toString() );
                            Log.e("description", new JSONObject( response.getJSONArray("weather").get(0).toString() ).optString("description"));
                            String weather = new JSONObject( response.getJSONArray("weather").get(0).toString() ).optString("description");

                            Log.e("temp", response.getJSONObject("main").optString("temp"));
                            String temp = response.getJSONObject("main").optString("temp");

                            String iconCode = new JSONObject( response.getJSONArray("weather").get(0).toString() ).optString("icon");
                            //load to imageview

                            Glide.with(Weather_Detail_Screen.this)
                                    .load("http://openweathermap.org/img/w/"+iconCode+".png") // image url
                                    .placeholder(null) // any placeholder to load at start
                                    .error(null)  // any image in case of error
                                    .override(200, 200) // resizing
                                    .centerCrop()
                                    .into(imageView);  // imageview object

                            //display values to recycler view
                            HashMap<String, String> labels = new Methods().fieldLables();
                            HashMap<String, String> fields = new Methods().JSONtoHash(response);
                            Log.e("labels", labels.toString());
                            Log.e("fields", fields.toString());
                            Log.e("labels.get(x)", labels.get("weather_icon").toString());
                            ArrayList<String> columnNames = db.getAllColumn(Tables.tbl_weather.table_name);

                            Weather_Details wthr;
                            for (int x = 0; x <= labels.size(); x++) {
                                wthr = new Weather_Details( labels.get( columnNames.get(x) ), fields.get (columnNames.get(x) ));
                                weatherList.add(wthr);
                            }

                            mAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            Log.e("errorCode", "onError errorCode : " + error.getErrorCode());
                            Log.e("errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.e("errorDetail", "onError errorDetail : " + error.getErrorDetail());

                        } else {
                            Log.e("errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }

                });
    }

    private void fetchOfflineData(String name){
        HashMap<String, String> labels = new Methods().fieldLables();
        HashMap<String, String> data =  db.getRecord("name = ?", new String[] {name}, Tables.tbl_weather.table_name);
        Log.e("data offline", data.toString());

        tv_message.setText(data.toString());

        ArrayList<String> columnNames = db.getAllColumn(Tables.tbl_weather.table_name);
        columnNames.remove(0);
        ArrayList<String> columnValues = db.getAllValue(Tables.tbl_weather.table_name, " WHERE name = \""+name+"\"");
        columnValues.remove(0);

        //add data to recyclerview
        Weather_Details wthr;

        Log.e("columnNames", columnNames.toString());
        Log.e("columnValues", columnValues.toString());
        for (int x = 0; x < columnNames.size(); x++) {
            wthr = new Weather_Details( labels.get(columnNames.get(x)), columnValues.get(x));
            weatherList.add(wthr);
        }

        mAdapter.notifyDataSetChanged();

    }

}