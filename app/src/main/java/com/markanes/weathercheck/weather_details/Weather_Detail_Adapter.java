package com.markanes.weathercheck.weather_details;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.markanes.weathercheck.R;

import java.util.List;

/**
 * Created by Mark on 10 Nov 2018.
 */

public class Weather_Detail_Adapter extends RecyclerView.Adapter<Weather_Detail_Adapter.MyViewHolder> {

    private List<Weather_Details> weathersList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView label, field;

        public MyViewHolder(View view) {
            super(view);

            label = (TextView) view.findViewById(R.id.tv_label);
            field = (TextView) view.findViewById(R.id.tv_field);

        }
    }


    public Weather_Detail_Adapter(List<Weather_Details> weathersList) {
        this.weathersList = weathersList;
    }

    @Override
    public Weather_Detail_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_details_list_row, parent, false);

        return new Weather_Detail_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Weather_Detail_Adapter.MyViewHolder holder, int position) {
        Weather_Details Weather = weathersList.get(position);
        holder.label.setText(Weather.getLabel());
        holder.field.setText(Weather.getField());

    }

    @Override
    public int getItemCount() {
        return weathersList.size();
    }
}

