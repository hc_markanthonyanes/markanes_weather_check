package com.markanes.weathercheck.weather_details;

/**
 * Created by Mark on 10 Nov 2018.
 */

public class Weather_Details {
    private String label;
    private String field;

    public Weather_Details() {
    }

    public Weather_Details(String label, String field) {
        this.label = label;
        this.field = field;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
