package com.markanes.weathercheck.methods;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Mark on 9 Nov 2018.
 */

public class Methods {

    public boolean isConnected(Context context) {
        ConnectivityManager conMgr =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null){
            Toast.makeText(context, "Not connected to internet", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }

    public HashMap<String, String> JSONtoHash(JSONObject response){
        HashMap<String, String> list = new HashMap<>();
        try {

            list.put("name_id", response.optString("id"));
            list.put("name", response.optString("name"));
            list.put("coord_lon", response.getJSONObject("coord").optString("lon"));
            list.put("coord_lat", response.getJSONObject("coord").optString("lat"));
            list.put("weather_description", new JSONObject( response.getJSONArray("weather").get(0).toString() ).optString("description"));
            list.put("weather_icon", new JSONObject( response.getJSONArray("weather").get(0).toString() ).optString("icon"));
            list.put("main_temp", response.getJSONObject("main").optString("temp"));
            list.put("main_pressure", response.getJSONObject("main").optString("pressure"));
            list.put("main_humidity", response.getJSONObject("main").optString("humidity"));
            list.put("wind_speed", response.getJSONObject("wind").optString("speed"));
            list.put("wind_deg", response.getJSONObject("wind").optString("deg"));
            list.put("clouds_all", response.getJSONObject("clouds").optString("all"));
            list.put("sys_sunrise", response.getJSONObject("sys").optString("sunrise"));
            list.put("sys_sunset", response.getJSONObject("sys").optString("sunset"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("list", list.toString());

        return list;
    }

    public HashMap<String, String> fieldLables(){
        HashMap<String, String> list = new HashMap<>();

//        list.put("id", "ID");
        list.put("name_id", "Name ID");
        list.put("name", "Nane");
        list.put("coord_lon", "Longtitude");
        list.put("coord_lat", "Latitude");
        list.put("weather_description", "Actual Weather");
        list.put("weather_icon", "Weather Icon");
        list.put("main_temp", "Temperature");
        list.put("main_pressure", "Pressure");
        list.put("main_humidity", "Humidity");
        list.put("wind_speed", "Wind Speed");
        list.put("wind_deg", "Wind Degree");
        list.put("clouds_all", "Clouds");
        list.put("sys_sunrise", "Sunrise");
        list.put("sys_sunset", "Sunset");

        Log.e("list", list.toString());

        return list;
    }

    public String spacer(String value){
        String new_str = value.replaceAll("_", " ");
        return new_str;
    }

}
