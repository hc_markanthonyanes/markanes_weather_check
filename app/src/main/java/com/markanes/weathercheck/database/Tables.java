package com.markanes.weathercheck.database;

/**
 * Created by Mark on 10 Nov 2018.
 */

public class Tables {

    public static abstract class tbl_weather {
        public static final String table_name = "weather";
        public static final String[] fields = {
                "id",
                "name_id",
                "name",
                "coord_lon",
                "coord_lat",
                "weather_description",
                "weather_icon",
                "main_temp",
                "main_pressure",
                "main_humidity",
                "wind_speed",
                "wind_deg",
                "clouds_all",
                "sys_sunrise",
                "sys_sunset",
        };
    }

}
