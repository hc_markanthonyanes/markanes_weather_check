package com.markanes.weathercheck.database;

import android.util.Log;

/**
 * Created by Mark on 10 Nov 2018.
 */

public class DatabaseTableObject {


    String table_name;
    String[] fields;
    String query;

    public DatabaseTableObject(String table_name, String[] fields) {

        this.table_name = table_name;
        this.fields = fields;

    }

    public String createQuery(){

        String create_fields = "";
        for(int i = 0; i < fields.length; i++){


            if(fields[i].contentEquals("id"))
                create_fields += fields[i] + " INTEGER,";
            else
                create_fields += fields[i] + " TEXT,";

        }

        query = "CREATE TABLE " + table_name + "(" + create_fields + "PRIMARY KEY("+ fields[0] + "));";

        Log.e("query", query);

        return query;
    }

    public String createQuery(String fields){

        query = "CREATE TABLE IF NOT EXISTS " + table_name + "(" + fields + "PRIMARY KEY("+ this.fields[0] + "));";

        Log.e("query", query);

        return query;
    }

    public String insertQuery(String values){

        String create_fields = "";

        for(int i = 0; i < fields.length; i++){

            create_fields += "`" + fields[i] + "`";

            if(i < (fields.length - 1))
                create_fields += ", ";
        }

        query = "INSERT INTO " + table_name + "(" + create_fields + ") VALUES" + values + ";";

        Log.e("query", query);

        return query;

    }

}
