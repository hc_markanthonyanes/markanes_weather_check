package com.markanes.weathercheck.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mark on 10 Nov 2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper{

    Context context;
    private static final String DATABASE_NAME = "weather_db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        Log.i("DatabaseHandler:", "constructed (created / opened)");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.i("DatabaseHandler:", "Creating Tables ...");

        //add tables
        DatabaseTableObject weather = new DatabaseTableObject(Tables.tbl_weather.table_name, Tables.tbl_weather.fields);

        //add queries
        db.execSQL(weather.createQuery());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * mark's method
     */

    /*
	 * reference
	 */
    public String getTest(String table){
        String result="";
        String selectQuery = "Select * FROM " + table;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int totalFields = cursor.getColumnCount() - 1; // loop starts with 0 while getColumnCount ends with the total number of fields
        int cid = cursor.getColumnIndex("record_id") - 1;
        String[] cols = cursor.getColumnNames();

        //result = String.valueOf( cols[0].toString() );
//		for (int a = 0; a < cols.length; a++){
//			result = result + " "+String.valueOf( cols[a].toString() );
//		}
        result = String.valueOf(cid);
        return result;
    }
    //row count of query
    public int getRowCount(String table){
        int cnt=0;
        String selectQuery = "SELECT rowid FROM " + table;
        Log.e("selectQuery", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        cnt = cursor.getCount();
        return cnt;
    }
    //return id's
    public ArrayList<String> getRowIDs(String table, String query){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select rowid FROM " + table + " WHERE "+query;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] cols = cursor.getColumnNames();
        String fieldComponent;
        //result = String.valueOf( cols[0].toString() );
        try{
            if(cursor.moveToFirst()){
                do{
                    for (int x = 0; x < cols.length; x++){
                        fieldComponent = cursor.getString( cursor.getColumnIndex(cols[x]) );
                        results.add(fieldComponent);
                    }

                }while(cursor.moveToNext());

            } else {
                Log.e("Error cursor iteration","Error cursor iteration");
                cursor.close();
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Field Name mismatch","One of your fieldname does not exist in sqlite db");
        } finally {
            db.close();
        }
        return results;
    }

    /**
     * get all column names (effective for creating blank records) skip id and record id index
     */
    public ArrayList<String> getAllColumn(String table){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select * FROM " + table;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] cols = cursor.getColumnNames();
        //result = String.valueOf( cols[0].toString() );
        for (int a = 0; a < cols.length; a++){
            results.add(String.valueOf( cols[a].toString() ));
        }
        return results;
    }

    /**
     * get all valuess based on column
     */
    public ArrayList<String> getAllValue(String table, String where){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select * FROM " + table +" "+ where;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] cols = cursor.getColumnNames();
        //variables
        String fieldComponent;
        try{

            if(cursor.moveToFirst()){
                do{

                    for (int x = 0; x < cols.length; x++){
                        fieldComponent = cursor.getString( cursor.getColumnIndex(cols[x]) );
                        results.add(fieldComponent);
                    }

                }while(cursor.moveToNext());

            } else {
                Log.e("Error cursor iteration","Error cursor iteration");
                cursor.close();
                results.add("No record found");
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Field Name mismatch","One of your fieldname does not exist in sqlite db");
            results.add("No record found");
        } finally {
            db.close();
        }
        return results;
    }

    //field only
    /*how to use
    String output = db.getRecord("fieldName", "tablename", "whereField", "whereValue" );
     */
    public String getRecord(String fieldName, String table, String whereField, String whereValue){
        String results = "";
        String selectQuery = "";
        if((whereField.contentEquals(""))&&(whereValue.contentEquals("")))
            selectQuery = "Select "+fieldName+" FROM " + table;
        else
            selectQuery = "Select "+fieldName+" FROM " + table + " WHERE "+whereField+" = \"" + whereValue + "\"";
        Log.v("SQL", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    results = cursor.getString( cursor.getColumnIndex(fieldName) );
                }while(cursor.moveToNext());

            } else {
                cursor.close();
            }
        }catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",fieldName);
        } finally {
            db.close();
        }
        return results;
    }

    //field only
    /*how to use
    String output = db.getRecordQuery("fieldName", "tablename", "query" );
     */
    public String getRecordQuery(String fieldName, String table, String whereQuery){
        String results = "";
        String selectQuery = "";
        selectQuery = "Select "+fieldName+" FROM " + table + " WHERE "+whereQuery;
        Log.v("SQL", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    results = cursor.getString( cursor.getColumnIndex(fieldName) );
                }while(cursor.moveToNext());

            } else {
                cursor.close();
            }
        }catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",fieldName);
        } finally {
            db.close();
        }
        return results;
    }

    //column only
    /*how to use
        ArrayList<String> output = db.getRecordColumn("columnName", "tablename", "whereField", "whereValue");
     */
    public ArrayList<String> getRecordColumn(String columnName, String table, String whereField, String whereValue){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "";
        String fieldComponent;
        if((whereField.contentEquals(""))&&(whereValue.contentEquals("")))
            selectQuery = "Select "+columnName+" FROM " + table;
        else
            selectQuery = "Select "+columnName+" FROM " + table + " WHERE "+whereField+" = \"" + whereValue + "\"";
        Log.e("query",selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    fieldComponent = cursor.getString( cursor.getColumnIndex(columnName) );
                    results.add(fieldComponent);
                }while(cursor.moveToNext());

            } else {
                cursor.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",columnName);
        } finally {
            db.close();
        }

        return results;
    }

    //column only
    /*how to use
        ArrayList<String> output = db.getRecordColumnQuery("columnName", "tablename", "whereQuery");
     */
    public ArrayList<String> getRecordColumnQuery(String columnName, String table, String whereQuery){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "";
        String fieldComponent;
        selectQuery = "Select "+columnName+" FROM " + table + " "+whereQuery;
        Log.e("query",selectQuery);


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    fieldComponent = cursor.getString( cursor.getColumnIndex(columnName) );
                    results.add(fieldComponent);
                }while(cursor.moveToNext());
            } else {
                cursor.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",columnName);
        } finally {
            db.close();
        }
        return results;
    }


    String nullToBlank(String s){
        if(s.contentEquals("null") || s.isEmpty() || s.length() == 0 ){
            s = "";
        }
        return s;
    }




    /***
     * HashMap<String, String> data = new HashMap<>();
     * data.put("app_account_first_name", "FNAME");
     * data.put("app_account_last_name", "LNAME");
     * databaseHandler.addRecord(data, Tables.tbl_weather.table_name);
     *
     * @param data hashmap of fields and values
     * @param table table where the query will be done
     */
    public void addRecord(HashMap<String, String> data, String table){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (Map.Entry<String, String> entry : data.entrySet()){

            values.put(entry.getKey(), entry.getValue());
            Log.i(entry.getKey(), entry.getValue());

        }

        Log.e("addRecord", table);

        db.insert(table, null, values);
        db.close();
    }

    /***
     *
     * HashMap<String, String> data = new HashMap<>();
     * data.put("app_account_first_name", "fname");
     * data.put("app_account_last_name", "lname");
     * databaseHandler.updateRecord(data, "id = ?", new String[] {"1"}, Tables.weather.table_name);
     *
     * @param data hashmap of fieldnames and updated values
     * @param whereClause conditions
     * @param whereArgs each ? in conditions will be taken from whereArgs
     * @param table table where the query will be done
     */
    public void updateRecord(HashMap<String, String> data, String whereClause, String[] whereArgs, String table){

        Log.i("updateRecord", whereClause);
        Log.i("updateRecord", Arrays.toString(whereArgs));
        Log.i("updateRecord", table);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (Map.Entry<String, String> entry : data.entrySet()){

            values.put(entry.getKey(), entry.getValue());
            Log.i(entry.getKey(), entry.getValue());

        }

        db.update(table, values, whereClause, whereArgs);
        db.close();

    }

    /***
     * HashMap<String, String> data =  databaseHandler.getRecord("id = ?", new String[] {"1"}, Tables.account_details.table_name);
     *
     * @param whereClause conditions
     * @param whereArgs each ? in conditions will be taken from whereArgs
     * @param table table where the query will be done
     * @return hashmap of fieldnames and values for that condition
     */
    public HashMap<String, String> getRecord(String whereClause, String[] whereArgs, String table){

        Log.e("query", " whereClause:" + whereClause + " whereArgs:" + Arrays.toString(whereArgs) + " table:" + table);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        HashMap<String, String> data = new HashMap<>();
        String selectQuery = "Select * FROM " + table;
        String query = selectQuery;

        if(!whereClause.equals(null)) {
            query = query + " WHERE " + whereClause;
            cursor = db.rawQuery(query, whereArgs);
        } else cursor = db.rawQuery(query, null);

        int columnCount = cursor.getColumnCount();

        if(cursor.moveToFirst())
            do
                for(int i = 0; i < columnCount; i++)
                    if(cursor.getString(i) != null)
                        data.put(cursor.getColumnName(i), cursor.getString(i));
            while (cursor.moveToNext());

        db.close();

        Log.e("data", data.toString());

        return data;

    }

}